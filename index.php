<?php
//die();
require 'vendor/autoload.php';

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

$locator = new \RocketTheme\Toolbox\ResourceLocator\UniformResourceLocator(__DIR__);
$locator->addPath('config', '', 'config');

$builder = new \UserFrosting\Config\ConfigPathBuilder($locator, 'config://');
$paths = $builder->buildPaths();
$loader = new \UserFrosting\Support\Repository\Loader\ArrayFileLoader($builder->buildPaths());

$config = new \UserFrosting\Support\Repository\Repository($loader->load());

if ($request->isMethod('post') && $request->isXmlHttpRequest()){
   $data = [
       'name' => $request->request->get('name'),
       'phone' => $request->request->get('phone'),
       'email' => $request->request->get('email'),
   ];

   $emailFrom = filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL) ? $request->request->get('email') : $config->get('config.email');

   $text = $config->get('config.email_text');
   foreach ($data as $key=>$value){
       $text = str_replace('{'.$key.'}',$value,$text);
   }

    $mail = new PHPMailer\PHPMailer\PHPMailer(false);
    //$mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->Host = $config->get('config.smtp.host');
    $mail->SMTPAuth = true;
   // dump($config->get('config.smtp.login')); die();
    $mail->Username = $config->get('config.smtp.login');
    $mail->Password = $config->get('config.smtp.password');
    $mail->SMTPSecure = 'tls';
    $mail->Port =$config->get('config.smtp.port');
    //$mail-

    $mail->setFrom($emailFrom, $data['name']);

    $mail->addAddress($config->get('config.email'), $config->get('config.shop_name'));     // Add a recipient

    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Contact form!';
    $mail->Body    = $text;

    $mail->send();

   $response = new \Symfony\Component\HttpFoundation\JsonResponse();
   $response->setData(['status'=>'success']);
   $response->send();
   die();
}
//die();
require_once 'main.html';

