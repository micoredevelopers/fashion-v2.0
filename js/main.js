$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 120
    }, 1500);
});

$(document).ready(function () {
    $('.inst_block .image_container img').click(function () {
        const $modalCont = $('#myModal');
        $modalCont.css('display', 'flex');
        $("#img01").attr('src', $(this).attr('data-img'));
    });
    wow = new WOW(
        {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
      })
      wow.init();
});

$('.btn_send button, .btn_discount, #modal_request .custom_btn').click(function (e) {
   if ($(this).parents('.form_container').find('input').val() === '') {
       $(this).parents('.form_container').find('input').addClass('error');
       e.preventDefault();
   } else if (!$(this).parents('.form_container').find('input').hasClass('error')) {
       let form = $(this).closest('form');
       let data = form.serialize();
       form.find('input').val('');
       $('#modal_request').modal('hide');
       $('#modal_thank').modal('show');
       $('head meta[name=viewport]').remove();
       $.post('/',data,function(response){
        
       });

   }

});

$('.form_container input').on('keypress', function () {
    if ($(this).val !== '') {
        $(this).removeClass('error');
    }
});

$('.program_text .custom_btn, .section_seminar_info .custom_btn, .bottom_request .custom_btn, .main_btn .btn_request').click(function () {
    $('#modal_request').modal('show');
});

// $('#modal_request .custom_btn').click(function () {
//     if ($(this).parents('.form_container').find('input').val() === '') {
//         $(this).parents('.form_container').find('input').addClass('error');
//         e.preventDefault();
//     } else if (!$(this).parents('.form_container').find('input').hasClass('error')) {
//         let form = $(this).closest('form');
//         let data = form.serialize();
//         form.find('input').val('');
//         $('#modal_request').modal('hide');
//         $('#modal_thank').modal('show');
//         $.post('/',data,function(response){
         
//         });
 
//     }
// });

$('.section_review_photo .image_container img').click(function () {
    const $modalCont = $('#myModal');
    $modalCont.css('display', 'flex');
    $("#img01").attr('src', $(this).attr('src'));
});

$('.close').on('click', function () {
    const $modalCont = $('#myModal');
    $modalCont.css('display', 'none');
});

$('.show_more').click(function () {
    $(this).toggleClass('isShow');
    $(this).parent().find('.list').toggleClass('fullShow');
});

// Set the date we're counting down to
var countDownDate = new Date("March 11, 2019 09:30:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    
  // Output the result in an element with id="demo"
//   document.getElementById("timer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
    $('#timer .days').find('.num').html(days);
    $('#timer .hours').find('.num').html(hours);
    $('#timer .minutes').find('.num').html(minutes);
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);

function zoomDisable(){
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="user-scalable=0"/>');
}

$("input[type=text], textarea").mouseover(zoomDisable);

